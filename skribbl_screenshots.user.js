// ==UserScript==
// @name         skribbl_screenshots
// @namespace    http://tampermonkey.net/
// @version      2024-05-22
// @description  try to take over the world!
// @author       wjwat
// @match        https://skribbl.io/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=skribbl.io
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    function slugify(str) {
        return String(str)
            .normalize('NFKD') // split accented characters into their base characters and diacritical marks
            .replace(/[\u0300-\u036f]/g, '') // remove all the accents, which happen to be all in the \u03xx UNICODE block.
            .trim() // trim leading or trailing whitespace
            .toLowerCase() // convert to lowercase
            .replace(/[^a-z0-9 -]/g, '') // remove non-alphanumeric characters
            .replace(/\s+/g, '-') // replace spaces with hyphens
            .replace(/-+/g, '-'); // remove consecutive hyphens
    }

    function getName() {
        const round = document.getElementById('game-round').innerText;
        const dateString = slugify(new Date().toJSON());
        const _playerName = document.querySelectorAll('div.drawing[style*="display: block"]')[0].parentNode.parentNode.parentNode.querySelector('.player-name').innerText.replace(' (You)', '');
        const playerName = typeof _playerName === 'string'
          ? _playerName
          : 'UNKNOWN'

        return `${dateString}_${slugify(round)}_${slugify(playerName)}`;
    }

    function downloadURI(uri, name) {
        let link = document.createElement("a");
        link.download = name;
        link.href = uri;
        link.click();
        link.remove();
    }

    document.addEventListener('keyup', (e) => {
        if (e.code === "F9") {
            const name = getName();
            document.getElementById('game-canvas').children[0].toBlob((b) => {
                let url = window.URL.createObjectURL(b);
                downloadURI(url, name);
                window.URL.revokeObjectURL(url);
            });
        }
    });
})();